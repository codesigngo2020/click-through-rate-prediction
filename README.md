# Click-through Rate Prediction

此文件紀錄 2022-09-20 ~ 2022-10-04 實作的 [Kaggle Click-through Rate Prediction](https://www.kaggle.com/competitions/avazu-ctr-prediction) 挑戰。

實作方式為在 Aws SageMaker 上 Training，將產出的 submission.csv 上傳至 Kaggle Dataset，透過 [Kaggle Notebook](https://www.kaggle.com/code/scott12055/ctr-prediction) 載入 submission.csv 後進行推論結果提交。

分為多個版本，v1 ~ v8，前 7 版為 XGBoost，最後的 v8 才採用 CatBoost。沒有對程式碼進行太多整理，尤其 v8（時間不太夠），透過以下說明整體過程。

Best Score (CatBoost)：
- Private Score：0.40762
- Public Score：0.40956

Best Score (XGBoost)：
- Private Score：0.53801
- Public Score：0.53978

## TODO
- 我有看到第 1 名（4 Idiots）有釋出程式碼，有稍微看過，可以近一步研究其特徵工程和採用的模型。

## 參考資料

- [CatBoost](https://catboost.ai/en/docs/)
- [4 Idiots' Approach for Click-through Rate Prediction](https://github.com/ycjuan/kaggle-avazu)

## 問題

Kaggle Notebook：https://www.kaggle.com/code/scott12055/ctr-prediction

### *1.* 探索資料分析 - 進行建模之前，請你描述觀察資料狀況。

*1.* 資料欄位和形態<br>
透過 DataFrame 讀取 train 資料時，可以看到 DataFrame 根據資料內容自動判定以下欄位為 object：site_id、site_domain、site_category、app_id、app_domain、app_category、device_id、device_ip、device_model，但根據官方 Data fields 說明中，可以知道以下被判定為 int64 的欄位也應該是類別資料：C1、banner_pos、device_type、device_conn_type、C14-C21。

*2.* 整體資料筆數<br>
整體 train 筆數約 4 千萬筆，Gzip 壓縮後的資料達 1.12 GB，這對記憶體是一大負擔，可採行以下策略應對：

- chunk 方式分批讀取。
- 僅讀取特定欄位進行資料處理。
- 選取正確資料型態，能用 uint8 就不要用 object 或是 int64。

*3.* 時間區間<br>
根據官方說明，train data 為 10 天的資料，且 Non-clicks 和 clicks 有經過 subsample 處理；而 test 資料為 1 天的資料。

- train data 為 2014/10/21 ~ 2014/10/30 數據；test 為 2014/10/31，時間有先後，需要做 window 處理。
- train data 中，clicks 約佔 17%，此比例不算低，但有可能需要 balance 正負樣本。

#### *1.1* 取 5% 資料進行探索

由於資料不小，所以採用 chunk 方式分批讀入，再抽樣 5% 進行小的資料集進行資料探索。

*4.* 各欄位唯一值的數量<br>
以下欄位的唯一值數量特別多：site_id、site_domain、site_category、app_id、app_domain、device_id、device_ip、device_model、C14、C17、C20，尤其 device_id 和 device_ip。

- 若採用 XGBoost，其模型有欄位必須為數值的限制，可採行的方式有：one-hot encoding、hashing、特徵轉換。
- 根據身在媒體工作的經驗，特定人的點擊率會比較高，這可能可以透過 device 資訊辨識個人。
- 兩個或多欄位可能有高度關聯性，這需要近一步探索。

*5.* 兩個欄位的關聯性<br>
以下為各欄位唯一值的數量，以及兩個欄位合併後取唯一值的數量（這部分是在之前探索中發現的，未留下程式碼），可能可以取一個欄位即可，或是合併欄位。

- site_id：3492；site_domain：4328；合併後約 7000。
- device_id：552801；device_ip：1875291；合併後約等於 device_ip 的數量。

官方文並無說明 device_id 和 device_ip 的取得方式以及所表示的資訊，但就以字面上應該可以推測：

- device_id：裝置的 unique id。
- device_ip：裝置的 ip 位置。因為 ip 可能為浮動 ip，所以一 device_id 可能有多個 device_ip；而單一公司內或同一網域內可能公用 ip，所以多個 device_id 也可能共用 device_ip，因此理論上為多對多關係。
- device_model、device_type：裝置的類型。

根據 device 資訊的推測，可能可以組合 device_id、device_ip、device_model、device_type 來辨識個人行為。

*6.* 不同特徵值的點擊率<br>
若採用 XGBoost 需要將類別資料轉換成數值型態，其中一種方式是轉成點擊率，這假設是：不同的值其點擊率會有明顯差異，例如某個 device_id 點擊率特別高。我們近一步將點擊率以 10% 一個區間進行觀察，可以看到 site_id、site_domain、app_id、add_domain、device_id、device_ip、device_model、device_model 有比較明顯的鑑別度，推測這樣的轉換應該某程度上是有效特徵。

#### *1.2* Test 資料探索

*7.* Test 資料未出現在 Train 中<br>
根據過往經驗，這種類別資料經常會遇到不在訓練資料中得情況，尤其是有時間性的資料，因此想觀察 Test 資料中各欄位的值有多少比例在 Train 資料中未出現過。但由於資料量過大，所以僅觀察 Test 和 Train 資料的唯一值數量。

常理上，唯一值數量很多的欄位應該某程度上都會有 Test 資料未出現在 Train 的情況，因此後續特徵工程有加上這個特徵。

### *2.* 特徵工程 - 建模用的 feature，你做了哪些處理？

*1.* 以 Window 的方式取 Train、Valid、Test 資料<br>
實務上，在預測第 N 天的結果時，使用的是 N-1 天前（含）的資料作為基礎，對第 N 天進行資料處理。因此以下為資料處理依據的日期以及實際訓練和預測的日期：

- Train：2014/10/21 ~ 2014/10/28，訓練 2014/10/29。
- Valid：2014/10/22 ~ 2014/10/29，預測 2014/10/30。
- Test：2014/10/23 ~ 2014/10/30，預測 2014/10/31。

*2.* 時間轉換<br>
- 將原本格式為 YYMMDDHH 的 hour 欄位轉為小時。
- 原本有取出 weekday，但後續不採用，而這在許多人的特徵工程中有出現的步驟。不採用的原因是，在這份資料集中，含 Test 資料，時間僅有 10 天，甚至不足 2 週，在這樣行跨時間過短的情況下，我認為 weekday 並不足以作為一個合理加入的特徵。

*3.* 點擊紀錄<br>
- 根據 device_id、device_ip、device_model、device_type 來辨識個人後，計算過往的點擊率 user_ctr。
- 計算該使用者是否在過往資料中出現過，以 is_user_impr 欄位呈現。

*4.* 特徵唯一值數量特別多的欄位轉換<br>
- 針對特徵唯一值數量特別多的欄位：site_id、site_domain、app_id、app_domain、C14、C17、C19、C20、C21 轉換為點擊率，並以 10% 為一區間。
- 計算該欄位值是否出現在過往資料。

*5.* Balance 正負資料集<br>
取全部的正樣本，並 subsample 負樣本，使正負樣本各佔一半。

### *3.* 模型調整 - 訓練完的模型會根據驗證資料進行優化調整，請說明優化的細節？

#### *3.1* XGBoost

*1.* 選擇重要欄位<br>
先 Train 了一個 XGBoost 模型，並取用 feature importance > 0.005 最為後續步驟的欄位採用。

*2.* 超參數進行調整<br>
為避免 Overfitting，針對 Subsample、Min child weight、Max depth、N estimators 等超參數進行調整，選擇當成效曲線開始趨緩時就不在增加模型複雜度，但成效差異幅度並不大，整體 roc_auc score 都落在 0.793 左右。

*3.* 資訊量流失<br>
在調整 Subsample 時，發現從 0.1 ~ 1.0 整體成效並無明顯差異，我推測這有兩種可能：

1. 因為資料的特性，不需要很大量資料即可訓練出好的結果。
2. 因為特徵工程上的第 4 步（特徵唯一值數量特別多的欄位轉換）造成資訊量流失，因此使用少量資料，甚至簡單的模型來學習即達到學習趨緩曲線，但為什麼 Valid 成效仍然很好，Test 卻一般般這讓我很困惑。

*4.* 選擇其他超參數<br>
最後使用 BayesianOptimization 調整 gamma、colsample_bytree、learning_rate、reg_alpha、reg_lambda 等參數。

#### *3.2* CatBoost

CatBoost 在 Train 完後並沒有做任何其他的調整。

### *4.* 思考細節 - 這些步驟也想了解你的思考的邏輯，為什麼會做這些處理和調整？

很多思考的細節在上述都有說明，這裡做更多補充以及提出一些未解的疑惑。

*1.* 選擇 CatBoost<br>
我認為在特徵工程的第 4 步（特徵唯一值數量特別多的欄位轉換），流失太多資訊量，因此在最後改用 CatBoost，利用其在類別處理上的優勢，結果也證明成效不錯。但這過程中產生幾個問題是我來不及處理的：

1. 我不確定若仍採用 XGBoost，但將類別欄位進行 hashing 處理是否會有一樣的效果？這可能需要深入研究 XGBoost 和 CatBoost 的差異，以及 CatBoost 是如何處理類別資訊的。
2. 當使用 XGBoost 時，特徵工程的第 3 和 4 步計算欄位值是否出現在過往資料，從 feature importance 可發現這些特徵是有被採用的：C21_is_exist、C17_is_exist、is_user_impr、C14_is_exist，這可以理解，因為當資訊量被消減時，某程度需要透過其他額外產生的資訊來補足。但當這些資訊送入 CatBoost 時，幾乎不採用，這讓我相當困惑。
3. XGBoost 的分數有一度到 0.57，多數時候都在 0.7 左右，我是覺得這是抽樣或是模型訓練的僥倖，相較於 CatBoost 穩定在 0.4 左右（但也才提交兩次）。

*2.* 實務上的可行性<br>
雖然有初步探索過這批 Test 資料集的使用者（依據 device_id、device_ip、device_model、device_type），是否在前幾天出現過，若有出現過在後續才能根據過往資料進行預測，我想這涉及幾點：

1. 要取前幾天的資料最為過往資料，實作中我是取前 8 天，然實際上可能因各網站（site_id、site_domain）而有差異，當使用者回訪的頻率越低時，需要的天數就月高。
2. 若實務上需要頻繁的訓練模型，以採用新進的使用者資料，或是資料複查度太高造成模型太大，進而增加預測時負擔，都需要考量是否進行資料工程的第 3 和 4 步，以達到降低推論時間，甚至可以在使用者端即時推論的邊緣運算概念。

*3.* 需不需要以 Window 取資料？<br>
我也參考了不少人釋出得 Notebook，看到許多人並非以 Window 方式取資料，雖然我認為以 Wiindow 方式比較符合實際邏輯，他們不使用的成效確實也不差。

*4.* 對 Tree 的理解<br>
就我初步的理解，決策樹類的模型，甚至簡單機器學習（相較於深度學習），都偏需要進行特徵工程去提供 summary 的資訊，例如：過往點擊成效。這點在 XGBoost 有比較明顯的成效，但縱使是 CatBoost，加上「點擊紀錄」的資訊都會讓成效提升。
